#Client-Server application


1. Server keeps ports 8000 and 8001 open.

2. Each client generates a unique identifier for itself and a message to send to a server.

3. Client connects to server port 8000, provides its unique identifier and gets a unique code from the server.

4. Client connects to server port 8001, provides its identifier, a text message, and code that it received on step 3.

5. If client code does not match client identifier, server returns an error to the client.

6. If client ID and client code are correct, server writes the provided text message to a log file.


Server can serve multiple clients at once (using Runnable interface). 

