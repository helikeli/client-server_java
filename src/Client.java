import java.io.*;
import java.net.Socket;
import java.util.*;

/**
 * Client side.
 * */
public class Client {

    private Socket authenticationSocket;
    private Socket connectionSocket;

    private BufferedReader connectionReader;
    private BufferedWriter connectionWriter;

    private BufferedReader authenticationReader;
    private BufferedWriter authenticationWriter;

    private String clientIdentifier;
    private String clientCode;

    private final String SERVER_OK = "100 OK";
    private final String SERVER_ERROR  = "303 SERVER ERROR DETECTED, TERMINATING CONNECTION";

    public Client(Socket connectionSocket, Socket authenticationSocket, String identifier) {
        try {
            this.connectionSocket = connectionSocket;
            this.authenticationSocket = authenticationSocket;
            this.clientIdentifier = identifier;
            setConnectionStreams();
            setAuthenticationStreams();
        } catch (IOException e){
            closeEverything();
        }
    }

    public void setConnectionStreams () throws IOException {
        this.connectionReader = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
        this.connectionWriter = new BufferedWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
    }

    public void setAuthenticationStreams() throws IOException {
        this.authenticationReader = new BufferedReader(new InputStreamReader(authenticationSocket.getInputStream()));
        this.authenticationWriter = new BufferedWriter(new OutputStreamWriter(authenticationSocket.getOutputStream()));
    }

    public void sendMessage(String string, BufferedWriter writer){

        try {
            System.out.println("Sending: " + string);
            writer.write(string);
            writer.newLine();
            writer.flush();
        } catch (IOException e){
            closeEverything();
        }
    }

    public String acceptMessage(BufferedReader reader){

        String msgFromServer = null;
        try {
            msgFromServer = reader.readLine();
            System.out.println("Recieved: " + msgFromServer);
        } catch (IOException e){
            closeEverything();
        }
        return msgFromServer;
    }

    public void closeEverything(){
        try {

            if (connectionReader != null) {
                connectionReader.close();
            }
            if (connectionWriter != null){
                connectionWriter.close();
            }
            if (connectionSocket != null){
                connectionSocket.close();
            }

            if (authenticationReader != null) {
                authenticationReader.close();
            }
            if (authenticationWriter != null){
                authenticationWriter.close();
            }
            if (authenticationSocket != null){
                authenticationSocket.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String generateMessage () {
        ArrayList <String> messages = new ArrayList<>
                (Arrays.asList("Hello, stranger!", "It was nice to be your client.", "Are you still using 3DES?? :O",
                        "Stream ciphers forever <3", "Hello, user!", "Hello, server!"));
        Random random = new Random();
        int randomInt = random.nextInt(messages.size() - 1);
        return messages.get(randomInt);
    }

    public boolean validateServerConfirmationMessage(String serverResponse) {

        if (!serverResponse.equals(SERVER_OK)) {
            sendMessage(SERVER_ERROR, authenticationWriter);
            closeEverything();
            return false;
        }
        return true;
    }

    public void run () throws ServiceConfigurationError {

        // message to send
        String message = generateMessage();
        String serverResponse = null;

        // send client ID and accept unique CODE from server
        sendMessage(clientIdentifier, connectionWriter);
        clientCode = acceptMessage(connectionReader);

        // send client ID
        sendMessage(clientIdentifier, authenticationWriter);
        serverResponse = acceptMessage(authenticationReader);
        if (!validateServerConfirmationMessage(serverResponse))
            throw new ServiceConfigurationError(SERVER_ERROR);

        // send client CODE
        sendMessage(clientCode, authenticationWriter);
        serverResponse = acceptMessage(authenticationReader);
        if (!validateServerConfirmationMessage(serverResponse))
            throw new ServiceConfigurationError(SERVER_ERROR);

        // send message
        sendMessage(message, authenticationWriter);
        closeEverything();

    }

    public static void main(String[] args) throws IOException {

        UUID uuid = UUID.randomUUID();

        Socket connectionSocket = new Socket("localhost", 8000);
        Socket authenticationSocket = new Socket("localhost", 8001);
        Client client = new Client(connectionSocket, authenticationSocket, uuid.toString());

        try {
            client.run();
        } catch (Exception | ServiceConfigurationError e ) {
            e.printStackTrace();
        }
    }
}


