import java.io.*;
import java.net.Socket;
import java.time.LocalTime;
import java.util.UUID;

/**
 * Handles connection and communication
 * with each client using Runnable interface.
 * (Runs from Server main method after new client
 * connects to server)
 *
 * */
public class ClientHandler implements Runnable {

    private Socket connectionSocket;
    private Socket authenticationSocket;

    private BufferedReader connectionReader;
    private BufferedWriter connectionWriter;
    private BufferedReader authenticationReader;
    private BufferedWriter authenticationWriter;

    private final String fileName = "./logs.txt";
    private PrintWriter fileWriter;

    private final String SERVER_ERROR_IN_CLIENT_ID = "301 CLIENT ID ERROR";
    private final String SERVER_ERROR_IN_CLIENT_CODE = "302 CLIENT CODE ERROR";
    private final String SERVER_OK = "100 OK";

    private String CLIENT_FIRST_ID;
    private String CLIENT_AUTHENTICATION_CODE;

    public ClientHandler(Socket connectionSocket, Socket authenticationSocket) {

        try {
            this.connectionSocket = connectionSocket;
            this.authenticationSocket = authenticationSocket;
            this.fileWriter = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)));
            setConnectionStreams();
            setAuthenticationStreams();

        }   catch (IOException e)  {
            closeSocketsAndStreams();
        }

    }

    public void setConnectionStreams () throws IOException {
        this.connectionReader = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
        this.connectionWriter = new BufferedWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
    }

    public void setAuthenticationStreams() throws IOException {
        this.authenticationReader = new BufferedReader(new InputStreamReader(authenticationSocket.getInputStream()));
        this.authenticationWriter = new BufferedWriter(new OutputStreamWriter(authenticationSocket.getOutputStream()));
    }

    public void acceptClientID() {
        CLIENT_FIRST_ID = acceptMessage(connectionReader);
    }

    public void generateAuthCode() {
        UUID clientCode = UUID.randomUUID();
        CLIENT_AUTHENTICATION_CODE = clientCode.toString();
    }

    public void sendAuthCode () {
        sendMessage(CLIENT_AUTHENTICATION_CODE, connectionWriter);
    }

    // receive client UID and client authentication CODE
    public void authenticate() {
        String clientID, clientCode;

        clientID = acceptMessage(authenticationReader);
        if (!CLIENT_FIRST_ID.equals(clientID)) {
            sendMessage(SERVER_ERROR_IN_CLIENT_ID, authenticationWriter);
            closeSocketsAndStreams();
        }
        else
            sendMessage(SERVER_OK, authenticationWriter);

        clientCode = acceptMessage(authenticationReader);

        if (!CLIENT_AUTHENTICATION_CODE.equals(clientCode)) {
            sendMessage(SERVER_ERROR_IN_CLIENT_CODE, authenticationWriter);
            closeSocketsAndStreams();
        }
        else
            sendMessage(SERVER_OK, authenticationWriter);
    }

    /**
     * Reads line from an buffered reader.
     * @param reader input stream to read from
     * @return String message from socket
     * */
    public String acceptMessage(BufferedReader reader) {

        String string = null;
        try {
            string = reader.readLine();
        } catch (IOException e) {
            closeSocketsAndStreams();
        }
        System.out.println("Recieved: " + string);
        return string;
    }

    /**
     * Sends line of text into given buffered writer
     * @param string string to be sent to the output stream
     * @param writer buffered output stream
     * */
    public void sendMessage(String string, BufferedWriter writer) {

        try {
            System.out.println("Sending: " + string);
            writer.write(string);
            writer.newLine();
            writer.flush();
        } catch (IOException e){
            closeSocketsAndStreams();
        }
    }

    public void writeToFile(String message, PrintWriter printWriter) {
        try {
            printWriter.println(LocalTime.now() + ": Client with ID " + CLIENT_FIRST_ID + " wrote: " + message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeFileWriter (PrintWriter fileWriter) {

        try {
            if (fileWriter != null)
                fileWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeSocketsAndStreams(){
        try {

            // close streams and connection on port 8000
            if (connectionReader != null) {
                connectionReader.close();
            }
            if (connectionWriter != null){
                connectionWriter.close();
            }
            if (connectionSocket != null){
                connectionSocket.close();
            }

            // close streams and connection on port 8001
            if (authenticationReader != null) {
                authenticationReader.close();
            }
            if (authenticationWriter != null){
                authenticationWriter.close();
            }
            if (authenticationSocket != null){
                authenticationSocket.close();
            }

        } catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

            try {
                acceptClientID();
                generateAuthCode();
                sendAuthCode();
                authenticate();
                String message = acceptMessage(authenticationReader);
                writeToFile(message, fileWriter);
                closeFileWriter(fileWriter);

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                closeSocketsAndStreams();
            }

    }
}

