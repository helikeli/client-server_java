import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Server side.
 * */
public class Server {

    private final ServerSocket serverConnectionSocket;
    private final ServerSocket serverAuthenticationSocket;
    private final String newClientNotification = "--New client has connected.--";

    public Server(ServerSocket serverConnectionSocket, ServerSocket serverAuthenticationSocket) {
        this.serverConnectionSocket = serverConnectionSocket;
        this.serverAuthenticationSocket = serverAuthenticationSocket;
    }

    public void startServer () {

        try {
            while (!serverConnectionSocket.isClosed() && !serverAuthenticationSocket.isClosed()){

                // wait for a client to connect
                Socket connectionSocket = serverConnectionSocket.accept();
                Socket authenticationSocket = serverAuthenticationSocket.accept();
                System.out.println(newClientNotification);
                ClientHandler clientHandler = new ClientHandler(connectionSocket, authenticationSocket);

                Thread thread = new Thread(clientHandler);
                thread.start();
            }
        } catch (IOException e) {
            closeServerSockets();
        }
    }

    public void closeServerSockets(){

        try {
            if (serverConnectionSocket != null)
                serverConnectionSocket.close();
            if (serverAuthenticationSocket != null)
                serverAuthenticationSocket.close();

        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket1 = new ServerSocket(8000);
        ServerSocket serverSocket2 = new ServerSocket(8001);
        Server server = new Server(serverSocket1, serverSocket2);
        server.startServer();
    }
}

